import App from "../../app";
import { createFighterImage } from "../fighterPreview";
import {showModal} from "./modal"
export function showWinnerModal(fighter) {
  const img = createFighterImage(fighter);
  const obj = {
    title: `${fighter.name} won the battle`,
    bodyElement: img,
  }
  showModal(obj) ;
  // call showModal function 
}
