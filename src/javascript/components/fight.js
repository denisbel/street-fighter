import {
    controls
} from '../../constants/controls';
export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        let leftBar = document.getElementById("left-fighter-indicator"),
            rigthBar = document.getElementById("right-fighter-indicator"),
            critText = document.getElementById("crit"),
            leftCritBar = document.getElementById("right-fighter-crit-indicator"),
            rightCritBar = document.getElementById("left-fighter-crit-indicator")
        let firstCritical = {
                q: NaN,
                w: NaN,
                e: NaN,
                isAble: true
            },
            secondCrtitical = {
                u: NaN,
                i: NaN,
                o: NaN,
                isAble: true
            };

        function Fighter(attack, defense, currentHP, critText, hpBar, maxHP, critBar) {
            this.attack = attack;
            this.defense = defense;
            this.currentHP = currentHP;
            this.critText = critText;
            this.block = false;
            this.hpBar = hpBar;
            this.maxHP = maxHP;
            this.critBar = critBar;
        }
        let leftFighter = new Fighter(firstFighter.attack, firstFighter.defense, firstFighter.health,
                [firstCritical, `${firstFighter.name} has broken ${secondFighter.name}'s bones `], leftBar, firstFighter.health, leftCritBar),
            rightFighter = new Fighter(secondFighter.attack, secondFighter.defense, secondFighter.health,
                [secondCrtitical, `${secondFighter.name} has broken ${firstFighter.name}'s bones `], rigthBar, secondFighter.health, rightCritBar);

        document.addEventListener('keydown', event => {
            switch (event.code) {
                case controls.PlayerOneAttack:
                    if (event.repeat || leftFighter.block) break;
                    hit(leftFighter, rightFighter);
                    if (rightFighter.currentHP < 0) resolve(firstFighter);
                    return;
                case controls.PlayerTwoAttack:
                    if (event.repeat || rightFighter.block) break;
                    hit(rightFighter, leftFighter);
                    if (leftFighter.currentHP < 0) resolve(secondFighter);
                    return;
                case controls.PlayerOneBlock:
                    leftFighter.block = true;
                    return;
                case controls.PlayerTwoBlock:
                    rightFighter.block = true;
                    return;
            }

            //First player critical hit check
            for (let i in controls.PlayerOneCriticalHitCombination) {
                if (controls.PlayerOneCriticalHitCombination[i] === event.code) {
                    if (leftFighter.block) return;
                    let button = controls.PlayerOneCriticalHitCombination[i].slice(-1).toLowerCase();
                    leftFighter.critText[0][button] = true;
                    break;
                }
            }
            criticalHit(leftFighter, rightFighter, critText);
            if (rightFighter.currentHP < 0) resolve(firstFighter);

            //Second player critical hit check
            for (let i in controls.PlayerTwoCriticalHitCombination) {
                if (controls.PlayerTwoCriticalHitCombination[i] === event.code) {
                    if (rightFighter.block) return;
                    let button = controls.PlayerTwoCriticalHitCombination[i].slice(-1).toLowerCase();
                    rightFighter.critText[0][button] = true;
                    break;
                }
            }
            criticalHit(rightFighter, leftFighter, critText);
            if (leftFighter.currentHP < 0) resolve(secondFighter);


        });
        document.addEventListener('keyup', event => {
            switch (event.code) {
                case controls.PlayerOneBlock:
                    leftFighter.block = false;
                    break;
                case controls.PlayerTwoBlock:
                    rightFighter.block = false;
                    break;
            }
            for (let i in controls.PlayerOneCriticalHitCombination) {
                if (controls.PlayerOneCriticalHitCombination[i] === event.code) {
                    let button = controls.PlayerOneCriticalHitCombination[i].slice(-1).toLowerCase();
                    leftFighter.critText[0][button] = false;
                    break;
                }
            }
            for (let i in controls.PlayerTwoCriticalHitCombination) {
                if (controls.PlayerTwoCriticalHitCombination[i] === event.code) {
                    let button = controls.PlayerTwoCriticalHitCombination[i].slice(-1).toLowerCase();
                    rightFighter.critText[0][button] = false;
                    break;
                }
            }
        });
    })
}

export function getDamage(attacker, defender) {
    const attack = getHitPower(attacker),
        defend = getBlockPower(defender);
    if (defend >= attack) return 0;
    return attack - defend;
    // return damage
}

export function getHitPower(fighter) {
    const crtiticalChance = getRandomArbitrary(1, 2);
    return fighter.attack * crtiticalChance;
    // return hit power
}

export function getBlockPower(fighter) {
    const dodgeChance = getRandomArbitrary(1, 2);
    return fighter.defense * dodgeChance;
    // return block power
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function hit(attacker, defender) {
    let damage = getDamage(attacker, defender);
    if (defender.block) damage = 0;
    defender.currentHP -= damage;
    defender.hpBar.style.width = `${(defender.currentHP*100)/defender.maxHP}%`;
}

function criticalHit(attacker, defender, critText) {
    for (let i in attacker.critText[0])
        if (!attacker.critText[0][i]) return;
    defender.currentHP -= attacker.attack * 2;
    defender.hpBar.style.width = `${(defender.currentHP*100)/defender.maxHP}%`;
    attacker.critText[0].isAble = false;
    (() => {
        setTimeout(function () {
            attacker.critText[0].isAble = true
        }, 10000);
    })();
    critText.innerHTML = attacker.critText[1];
    (() => {
        setTimeout(function () {
            critText.innerHTML = ""
        }, 1500);
    })();
    defender.critBar.style.width = `0%`;
    for (let i = 1; i < 11; ++i) {
        (() => {
            setTimeout(function () {
                defender.critBar.style.width = `${i*10}%`
            }, i * 1000);
        })();
    }


}