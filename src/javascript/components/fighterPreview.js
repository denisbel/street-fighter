import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if(fighter==null) return "";
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  const img = createFighterImage(fighter);
  fighterElement.append(img);
  const ul = createElement({
    tagName: 'ul',
    className: "",
  });
  const name = createElement({
    tagName: 'li',
    className: "",
  });
  name.innerHTML = `Name: ${fighter.name}`;
  ul.append(name);
  const defense = createElement({
    tagName: 'li',
    className: "",
  });
  defense.innerHTML = `Defense: ${fighter.defense}`;
  ul.append(defense);
  const attack = createElement({
    tagName: 'li',
    className: "",
  });
  attack.innerHTML = `Attack: ${fighter.attack}`;
  ul.append(attack);
  const health = createElement({
    tagName: 'li',
    className: "",
  });
  health.innerHTML = `Health: ${fighter.health}`;
  ul.append(health);
  
  fighterElement.append(ul);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
